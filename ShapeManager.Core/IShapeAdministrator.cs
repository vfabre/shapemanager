﻿using System;
using Ninject;
using System.Collections.Generic;
namespace ShapeManager.Core
{
    public interface IShapeAdministrator : IEnumerable<IShape>
    {
        void Add(IShape shape);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShapeManager.Core.Extentions
{
    public static class Extentions
    {
        public static bool IsNull(this object param)
        {
            return param == null;
        }

        public static bool IsNotNull(this object param)
        {
            return param != null;
        }
    }
}

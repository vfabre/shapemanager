﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ninject;
using System.Diagnostics.Contracts;
using ShapeManager.Core.Extentions;

namespace ShapeManager.Core
{
    public class ShapeAdministrator : IShapeAdministrator
    {
        ICollection<IShape> shapes { get; set; }

        public ShapeAdministrator()
        {
            shapes = new List<IShape>();
        }

        /// <summary>
        /// Add the shape to the repository shapes
        /// </summary>
        /// <param name="shape"></param>
        public void Add(IShape shape)
        {
            shapes.Add(shape);
        }

        #region Implementation of IEnumerable
        public IEnumerator<IShape> GetEnumerator()
        {
            return shapes.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        #endregion
    }
}

﻿using System;
using System.Diagnostics.Contracts;
using System.Text;
using ShapeManager.Core.Extentions;

namespace ShapeManager.Core
{
    public class Donut : Shape
    {
        public double MinorRadius { get; set; }
        public double MayorRadius { get; set; }

        public Donut(double coordinatex, double coordinatey, double mayorRadius, double minorRadius)
            : base(coordinatex, coordinatey)
        {
            MayorRadius = mayorRadius;
            MinorRadius = minorRadius;
        }

        public override double CalculateArea()
        {
            return 4 * Math.PI * MayorRadius * MinorRadius;
        }

        public override string ToString()
        {
            return string.Format(Messages.DonutToString,
                CoordinateX, CoordinateY, MayorRadius, MinorRadius);
        }
    }
}

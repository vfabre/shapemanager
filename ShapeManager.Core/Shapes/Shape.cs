﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShapeManager.Core
{
    public abstract class Shape : IShape
    {
        public double CoordinateX { get; set; }
        public double CoordinateY { get; set; }
        public string Name { get; protected set; }

        public Shape(double coordinatex, double coordinatey)
        {
            CoordinateX = coordinatex;
            CoordinateY = coordinatey;
        }

        /// <summary>
        /// Return the shape area
        /// </summary>
        /// <returns>System.Double</returns>
        public abstract double CalculateArea();

        /// <summary>
        /// Returns a description of a shape
        /// </summary>
        /// <returns>System.String</returns>
        public abstract override string ToString();
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.Contracts;
using ShapeManager.Core.Extentions;

namespace ShapeManager.Core
{
    public class Square : Shape
    {
        public double Lenght { get; set; }

        public Square(double coordinatex, double coordinatey, double lenght)
            : base(coordinatex, coordinatey)
        {
            Lenght = lenght;
        }

        public override double CalculateArea()
        {
            return Math.Pow(Lenght, 2);
        }

        public override string ToString()
        {
            return string.Format(Messages.SquareToString,
                       CoordinateX, CoordinateY, Lenght);
        }
    }
}

﻿using System;
using System.Diagnostics.Contracts;
using System.Text;
using ShapeManager.Core.Extentions;

namespace ShapeManager.Core
{
    public class Circle : Shape
    {
        public double Radius { get; set; }

        public Circle(double coordinatex, double coordinatey, double radius)
            : base(coordinatex, coordinatey)
        {
            Radius = radius;
        }

        public override string ToString()
        {
            return string.Format(Messages.CircleToString,
                CoordinateX, CoordinateY, Radius);
        }

        public override double CalculateArea()
        {
            return Math.PI * Math.Pow(Radius, 2);
        }
    }
}

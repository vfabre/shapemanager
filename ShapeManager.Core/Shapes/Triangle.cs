﻿using System.Diagnostics.Contracts;
using ShapeManager.Core.Extentions;
using System.Text;

namespace ShapeManager.Core
{
    public class Triangle : Shape
    {
        public double CoordinateBX { get; set; }
        public double CoordinateBY { get; set; }

        public double CoordinateCX { get; set; }
        public double CoordinateCY { get; set; }

        public Triangle(double coordinatex,
            double coordinatey, double coordinateBx, double coordinateBy, double coordinateCx, double coordinateCy)
            : base(coordinatex, coordinatey)
        {
            this.CoordinateBX = coordinateBx;
            this.CoordinateBY = coordinateBy;
            this.CoordinateCX = coordinateCx;
            this.CoordinateCY = coordinateCy;
        }

        /// <summary>
        /// http://www.mathopenref.com/coordtrianglearea.html
        /// </summary>
        /// <returns></returns>
        public override double CalculateArea()
        {
            return (CoordinateX * (CoordinateBY - CoordinateCY) +
                CoordinateBX * (CoordinateCY - CoordinateY) +
                CoordinateCX * (CoordinateY - CoordinateBY)) / 2;
        }

        public override string ToString()
        {
            return string.Format(Messages.TriangleToString,
                CoordinateX,
                CoordinateY,
                CoordinateBX,
                CoordinateBY,
                CoordinateCX,
                CoordinateCY);
        }
    }
}

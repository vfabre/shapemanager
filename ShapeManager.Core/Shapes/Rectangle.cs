﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.Contracts;
using ShapeManager.Core.Extentions;


namespace ShapeManager.Core
{
    public class Rectangle : Shape
    {
        public double LenghtA { get; set; }
        public double LenghtB { get; set; }

        public Rectangle(double coordinatex, double coordinatey, double lenghtA, double lenghtB)
            : base(coordinatex, coordinatey)
        {
            LenghtA = lenghtA;
            LenghtB = lenghtB;
        }
        
        public override double CalculateArea()
        {
            return LenghtA * LenghtB;
        }

        public override string ToString()
        {
            return string.Format(Messages.RectangleToString,
                CoordinateX, CoordinateY, LenghtA, LenghtB);
        }
    }
}

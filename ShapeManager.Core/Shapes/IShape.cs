﻿using System;
namespace ShapeManager.Core
{
    public interface IShape
    {
        double CoordinateX { get; set; }
        double CoordinateY { get; set; }
        double CalculateArea();
    }
}

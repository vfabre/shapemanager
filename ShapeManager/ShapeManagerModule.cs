﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ninject.Modules;
using ShapeManager.Commands;
using ShapeManager.Core;

namespace ShapeManager
{
    public class ShapeManagerModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IShapeAdministrator>().To<ShapeAdministrator>().InSingletonScope();

            //Commands
            Bind<CircleCommand>().ToSelf();
            Bind<DonutCommand>().ToSelf();
            Bind<RectangleCommand>().ToSelf();
            Bind<SquareCommand>().ToSelf();
            Bind<TriangleCommand>().ToSelf();
            Bind<ListCommand>().ToSelf();
            //Bind<IShape>().To<Circle>().Named(ShapeNames.Circle);
            //Bind<IShape>().To<Donut>().Named(ShapeNames.Donut);
            //Bind<IShape>().To<Rectangle>().Named(ShapeNames.Rectangle);
            //Bind<IShape>().To<Square>().Named(ShapeNames.Square);
            //Bind<IShape>().To<Triangle>().Named(ShapeNames.Triangle);
        }
    }
}

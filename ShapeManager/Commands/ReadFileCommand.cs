﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConsoleFx.Programs.Interactive;
using ShapeManager.Core;
using System.IO;

namespace ShapeManager.Commands
{
    [Command(Name = "read-file",
        ShortName = "file",
        Description = "Read a file a insert all figures in the list.",
        HelpDescription = @" read-file <file path> 
    e.g. read-file c:\myshapes.txt")]
    public class ReadFileCommand : CommandBase
    {
        private static string filePath = null;
        IShapeAdministrator shapeAdministrator;

        public ReadFileCommand(IShapeAdministrator shapeAdministrator)
        {
            this.shapeAdministrator = shapeAdministrator;
        }

        public override void Execute()
        {
            Console.SetIn(new StreamReader(filePath));
            
        }

        public override IEnumerable<ShellArgument> GetArguments()
        {
            yield return AddArgument(true)
                .AssignTo(() => filePath);
        }
    }
}

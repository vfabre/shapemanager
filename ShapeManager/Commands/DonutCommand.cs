﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConsoleFx.Programs.Interactive;
using ConsoleFx.Validators;
using ShapeManager.Core;

namespace ShapeManager.Commands
{
    [Command(Name = "donut",
        ShortName = "d",
        Description = "Adds a new donut to the repository",
        HelpDescription = @"donut <coordinate x> <coordinate y> <mayor radius> <minor radius> 
    e.g. donut 4.5 7.8 1.5 1.8")]
    public sealed class DonutCommand : CommandBase
    {
        private static double coordinatex = double.MinValue;
        private static double coordinatey = double.MinValue;
        private static double mayorRadius = double.MinValue;
        private static double minorRadius = double.MinValue;
        IShapeAdministrator shapeAdministrator;

        public DonutCommand(IShapeAdministrator shapeAdministrator)
        {
            this.shapeAdministrator = shapeAdministrator;
        }

        public override void Execute()
        {
            var donut = new Donut(coordinatex, coordinatey, mayorRadius, minorRadius);
            shapeAdministrator.Add(donut);
            Console.WriteLine(donut);
        }

        public override IEnumerable<ShellArgument> GetArguments()
        {
            yield return AddArgument(true)
                .AssignTo(() => coordinatex);
            yield return AddArgument(true)
                .AssignTo(() => coordinatey);
            yield return AddArgument(true)
                .AssignTo(() => mayorRadius);
            yield return AddArgument(true)
                .AssignTo(() => minorRadius);
        }
    }
}

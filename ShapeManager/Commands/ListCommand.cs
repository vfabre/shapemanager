﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConsoleFx.Programs.Interactive;
using ShapeManager.Core;

namespace ShapeManager.Commands
{
     [Command(Name = "list",
        ShortName = "lst",
        Description = "Return a list with all shapes in memory",
        HelpDescription = @"list")]
    public sealed class ListCommand : CommandBase
    {
        IShapeAdministrator shapeAdministrator;

        public ListCommand(IShapeAdministrator shapeAdministrator)
        {
            this.shapeAdministrator = shapeAdministrator;
        }

        public override void Execute()
        {
            foreach (var shape in shapeAdministrator)
            {
                Console.WriteLine(shape);
            }
        }
    }
}

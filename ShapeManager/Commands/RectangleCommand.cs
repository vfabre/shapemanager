﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConsoleFx.Programs.Interactive;
using ConsoleFx.Validators;
using ShapeManager.Core;

namespace ShapeManager.Commands
{
    [Command(Name = "rectangle",
        ShortName = "r",
        Description = "Adds a new rectangle to the repository",
        HelpDescription = @"rectangle <coordinate x> <coordinate y> <lenght side a> <lenght side b>
    e.g. rectangle 3.5 2.0 5.6 7.2")]
    public sealed class RectangleCommand : CommandBase
    {
        private static double coordinatex = double.MinValue;
        private static double coordinatey = double.MinValue;
        private static double lenghtA = double.MinValue;
        private static double lenghtB = double.MinValue;
        IShapeAdministrator shapeAdministrator;

        public RectangleCommand(IShapeAdministrator shapeAdministrator)
        {
            this.shapeAdministrator = shapeAdministrator;
        }

        public override void Execute()
        {
            var rectangle = new Rectangle(coordinatex, coordinatey, lenghtA, lenghtB);
            shapeAdministrator.Add(rectangle);
            Console.WriteLine(rectangle);
        }

        public override IEnumerable<ShellArgument> GetArguments()
        {
            yield return AddArgument(true)
                 .AssignTo(() => coordinatex);
            yield return AddArgument(true)
                .AssignTo(() => coordinatey);
            yield return AddArgument(true)
                .AssignTo(() => lenghtA);
            yield return AddArgument(true)
                .AssignTo(() => lenghtB);
        }
    }
}

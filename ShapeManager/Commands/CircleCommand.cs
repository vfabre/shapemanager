﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConsoleFx.Programs.Interactive;
using ConsoleFx.Validators;
using ShapeManager.Core;

namespace ShapeManager.Commands
{
    [Command(Name = "circle",
        ShortName = "c",
        Description = "Adds a new circle to the repository",
        HelpDescription = @" circle <coordinate x> <coordinate y> <radius> 
    e.g. circle 1.7 -5.05 6.9")]
    public sealed class CircleCommand : CommandBase
    {
        private static double coordinatex = double.MinValue;
        private static double coordinatey = double.MinValue;
        private static double radius = double.MinValue;
        IShapeAdministrator shapeAdministrator;

        public CircleCommand(IShapeAdministrator shapeAdministrator)
        {
            this.shapeAdministrator = shapeAdministrator;
        }

        public override void Execute()
        {
            var circle = new Circle(coordinatex, coordinatey, radius);
            shapeAdministrator.Add(circle);
            Console.WriteLine(circle);
        }

        public override IEnumerable<ShellArgument> GetArguments()
        {
            yield return AddArgument(true)
                .AssignTo(() => coordinatex);
            yield return AddArgument(true)
                .AssignTo(() => coordinatey);
            yield return AddArgument(true)
                .AssignTo(() => radius);
        }
    }
}

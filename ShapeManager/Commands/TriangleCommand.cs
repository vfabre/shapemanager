﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConsoleFx.Programs.Interactive;
using ConsoleFx.Validators;
using ShapeManager.Core;

namespace ShapeManager.Commands
{
    [Command(Name = "triangle",
        ShortName = "t",
        Description = "Adds a new triangle to the repository",
        HelpDescription = @"triangle <coordinate Ax> <coordinate Ay> <coordinate Bx> <coordinate By> <coordinate Cx> <coordinate Cy>
    e.g. triangle 4.5 1 -2.5 -33 23 0.3")]
    public sealed class TriangleCommand : CommandBase
    {
        private static double coordinatex = double.MinValue;
        private static double coordinatey = double.MinValue;
        private static double coordinateBx = double.MinValue;
        private static double coordinateBy = double.MinValue;
        private static double coordinateCx = double.MinValue;
        private static double coordinateCy = double.MinValue;
        IShapeAdministrator shapeAdministrator;

        public TriangleCommand(IShapeAdministrator shapeAdministrator)
        {
            this.shapeAdministrator = shapeAdministrator;
        }

        public override void Execute()
        {
            var triangle = new Triangle(coordinatex, coordinatey, coordinateBx, coordinateBy, coordinateCx, coordinateCy);
            shapeAdministrator.Add(triangle);
            Console.WriteLine(triangle);
        }

        public override IEnumerable<ShellArgument> GetArguments()
        {
            yield return AddArgument(true)
                .AssignTo(() => coordinatex);
            yield return AddArgument(true)
                .AssignTo(() => coordinatey);
            yield return AddArgument(true)
                .AssignTo(() => coordinateBx);
            yield return AddArgument(true)
                .AssignTo(() => coordinateBy);
            yield return AddArgument(true)
                .AssignTo(() => coordinateCx);
            yield return AddArgument(true)
                .AssignTo(() => coordinateCy);
        }
    }
}

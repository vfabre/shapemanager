﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConsoleFx.Programs.Interactive;
using ConsoleFx.Validators;
using ShapeManager.Core;

namespace ShapeManager.Commands
{
    [Command(Name = "square",
        ShortName = "s",
        Description = "Adds a new square to the repository",
        HelpDescription = @"square <coordinate x> <coordinate y> <length>
    e.g. square 3.55 4.1 2.77")]
    public sealed class SquareCommand : CommandBase
    {
        private static double coordinatex = double.MinValue;
        private static double coordinatey = double.MinValue;
        private static double lenght = double.MinValue;
        IShapeAdministrator shapeAdministrator;

        public SquareCommand(IShapeAdministrator shapeAdministrator)
        {
            this.shapeAdministrator = shapeAdministrator;
        }

        public override void Execute()
        {
            var square = new Square(coordinatex, coordinatey, lenght);
            shapeAdministrator.Add(square);
            Console.WriteLine(square);
        }

        public override IEnumerable<ShellArgument> GetArguments()
        {
            yield return AddArgument(true)
                .AssignTo(() => coordinatex);
            yield return AddArgument(true)
                .AssignTo(() => coordinatey);
            yield return AddArgument(true)
                .AssignTo(() => lenght);
        }
    }
}

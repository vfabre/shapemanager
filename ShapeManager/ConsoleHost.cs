﻿using System;
using System.IO;
using System.Linq;
using ConsoleFx;
using ConsoleFx.Programs.Interactive;
using ConsoleFx.Programs.Interactive.Commands;
using ConsoleFx.Programs.Simple;
using ConsoleFx.Validators;
using System.Threading;

namespace ShapeManager
{
    public class ConsoleHost
    {
        private static string filePath = null;

        public ConsoleHost(string[] args)
        {
            if (args.Any())
            {
                //ExecuteFileProcess();
            }
            else
            {
                ExecuteInteractive();
            }
        }

        private void ExecuteInteractive()
        {
            var shell = new InteractiveShell
            {
                Prompt = "shapemanager> ",
                SpaceBeforeCommand = true,
                SpaceAfterCommand = true,
                PromptForeColor = ConsoleColor.White,
                PromptBackColor = ConsoleColor.DarkBlue,
                ForeColor = ConsoleColor.White,
                BackColor = ConsoleColor.DarkBlue,
                ClearScreenOnStart = true,
                OnStartup = PrintHeader,

            };
            shell.AddHelpSupport();
            shell.AddBuiltinCommand(BuiltinCommands.Cls);
            shell.Run();
        }

        private static void PrintHeader(InteractiveShell shell)
        {
            ConsoleEx.WriteLine(ConsoleColor.White, ConsoleColor.DarkBlue, "ShapeManager Interactive Shell");
            ConsoleEx.WriteLine(ConsoleColor.White, ConsoleColor.DarkBlue, "Type HELP for help on available commands");
            ConsoleEx.WriteLine(ConsoleColor.White, ConsoleColor.DarkBlue, "your current culture is {0}. Keep this in mind for the comma delimiters", Thread.CurrentThread.CurrentCulture.Name);

            ConsoleEx.WriteBlankLine();
        }
    }
}

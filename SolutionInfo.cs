#region --- License & Copyright Notice ---
/*
ConsoleFx CommandLine Processing Library

Copyright (c) 2006-2011 Jeevan James
All rights reserved.

The contents of this file are made available under the terms of the
Eclipse Public License v1.0 (the "License") which accompanies this
distribution, and is available at the following URL:
http://opensource.org/licenses/eclipse-1.0.txt

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either expressed or implied. See the License for
the specific language governing rights and limitations under the License.

By using this software in any fashion, you are agreeing to be bound by the
terms of the License.
*/
#endregion

using System;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

//General assembly information
[assembly: AssemblyCompany("Jeevan James")]
[assembly: AssemblyProduct("ConsoleFx Commandline Processing Library")]
[assembly: AssemblyCopyright("Copyright (c) 2006-2011 Jeevan James")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

//Assembly version information
[assembly: AssemblyVersion("1.0.0.0")]

//CLS compliance. True by default. Turn off specifically for code that is not compliant.
[assembly: CLSCompliant(true)]

//Visibility to COM. False by default. Turn on specifically for code that needs to be
//visible to COM.
[assembly: ComVisible(false)]

[assembly: NeutralResourcesLanguage("en-US")]
﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShapeManager.Core;
using Ninject;

namespace ShapeManager.Test
{
    [TestClass]
    public class ShapeAdministratorTest
    {
        private ShapeAdministrator administrator;

        [TestInitialize]
        public void Initialize()
        {
            administrator = new ShapeAdministrator();
        }

        [TestMethod]
        public void WhenShapeIsCreatedWithTriangleParameterReturnATriangle()
        {
            var coordinateX = 4.5;
            var coordinateY = 1;
            var coordinateBX = -2.5;
            var coordinateBY = -33;
            var coordinateCX = 23;
            var coordinateCY = 0.3;
            var triangle = new Triangle(
                coordinateX, coordinateY, coordinateBX, coordinateBY, coordinateCX, coordinateCY);

            var returnMessage = string.Format(Messages.TriangleToString,
                coordinateX, coordinateY, coordinateBX, coordinateBY, coordinateCX, coordinateCY);

            Assert.AreEqual(typeof(Triangle), triangle.GetType());
            Assert.AreEqual(returnMessage, triangle.ToString());
        }

        [TestMethod]
        public void WhenShapeIsCreatedWithCircleParameterReturnACircle()
        {
            var coordinateX = 1.7;
            var coordinateY = 5.05;
            var radius = 6.9;
            var returnMessage = string.Format(Messages.CircleToString, coordinateX, coordinateY, radius);

            var cicle = new Circle(coordinateX, coordinateY, radius);

            Assert.AreEqual(typeof(Circle), cicle.GetType());
            Assert.AreEqual(returnMessage, cicle.ToString());
        }

        [TestMethod]
        public void WhenShapeIsDonutWithDonutParameterReturnADonut()
        {
            var coordinateX = 4.5;
            var coordinateY = 7.8;
            var mayorRadius = 1.5;
            var minorRadius = 1.8;
            var donut = new Donut(coordinateX, coordinateY, mayorRadius, minorRadius);

            var returnMessage = string.Format(Messages.DonutToString, coordinateX, coordinateY, mayorRadius, minorRadius);

            Assert.AreEqual(typeof(Donut), donut.GetType());
            Assert.AreEqual(returnMessage, donut.ToString());
        }

        [TestMethod]
        public void WhenShapeIsDonutWithRectangleParameterReturnARectangle()
        {
            var coordinateX = 3.5;
            var coordinateY = 2.0;
            var lenghtA = 5.6;
            var lenghtB = 7.2;
            var returnMessage = string.Format(Messages.RectangleToString, coordinateX, coordinateY, lenghtA, lenghtB);

            var rectangle = new Rectangle(coordinateX, coordinateY, lenghtA, lenghtB);

            Assert.AreEqual(typeof(Rectangle), rectangle.GetType());
            Assert.AreEqual(returnMessage, rectangle.ToString());
        }

        [TestMethod]
        public void WhenShapeIsDonutWithSquareParameterReturnASquare()
        {
            var coordinateX = 3.55;
            var coordinateY = 4.1;
            var lenght = 2.77;

            var returnMessage = string.Format(Messages.SquareToString, coordinateX, coordinateY, lenght);

            var square = new Square(coordinateX, coordinateY, lenght);

            Assert.AreEqual(typeof(Square), square.GetType());
            Assert.AreEqual(returnMessage, square.ToString());
        }

    }
}

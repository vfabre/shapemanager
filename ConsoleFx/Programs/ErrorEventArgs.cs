using System;

namespace ConsoleFx.Programs
{
    /// <summary>
    /// Provides exception details for the BeforeError and AfterError events of ConsoleProgram classes
    /// </summary>
    public sealed class ErrorEventArgs : EventArgs
    {
        private readonly Exception _exception;

        internal ErrorEventArgs(Exception exception)
        {
            _exception = exception;
        }

        /// <summary>
        /// The exception that is being handled
        /// </summary>
        public Exception Exception
        {
            get { return _exception; }
        }
    }
}
#region --- License & Copyright Notice ---
/*
ConsoleFx CommandLine Processing Library

Copyright (c) 2006-2012 Jeevan James
All rights reserved.

The contents of this file are made available under the terms of the
Eclipse Public License v1.0 (the "License") which accompanies this
distribution, and is available at the following URL:
http://opensource.org/licenses/eclipse-1.0.txt

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either expressed or implied. See the License for
the specific language governing rights and limitations under the License.

By using this software in any fashion, you are agreeing to be bound by the
terms of the License.
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq.Expressions;

using ConsoleFx.Parsers;
using ConsoleFx.Validators;

namespace ConsoleFx.Programs.Complex
{
    public static class ComplexFluentExtensions
    {
        #region Argument extensions
        public static ComplexArgument ValidateWith(this ComplexArgument argument, params BaseValidator[] validators)
        {
            FluentArgumentHelpers.ValidateWith(argument, validators);
            return argument;
        }

        public static void HandledBy(this ComplexArgument argument, ArgumentHandler handler)
        {
            FluentArgumentHelpers.HandledBy(argument, handler);
        }

        public static void AssignTo<T>(this ComplexArgument argument, Expression<Func<T>> expression, Converter<string, T> converter = null)
        {
            FluentArgumentHelpers.AssignTo(argument, expression, converter);
        }
        #endregion

        #region Option extensions
        public static ComplexOption ValidateWith(this ComplexOption option, params BaseValidator[] validators)
        {
            FluentOptionHelpers.ValidateWith(option, validators);
            return option;
        }

        public static ComplexOption ValidateWith(this ComplexOption option, int parameterIndex, params BaseValidator[] validators)
        {
            FluentOptionHelpers.ValidateWith(option, parameterIndex, validators);
            return option;
        }

        public static ComplexOption HandledBy(this ComplexOption option, OptionHandler handler)
        {
            FluentOptionHelpers.HandledBy(option, handler);
            return option;
        }

        public static ComplexOption Flag(this ComplexOption option, Expression<Func<bool>> expression)
        {
            FluentOptionHelpers.Flag(option, expression);
            return option;
        }

        public static ComplexOption AssignTo<T>(this ComplexOption option, Expression<Func<T>> expression, Converter<string, T> converter = null,
            int parameterIndex = ParameterIndex.First)
        {
            FluentOptionHelpers.AssignTo(option, expression, converter, parameterIndex);
            return option;
        }

        public static ComplexOption AddToList<T>(this ComplexOption option, Expression<Func<IList<T>>> expression,
            Converter<string, T> converter = null, int parameterIndex = ParameterIndex.All)
        {
            FluentOptionHelpers.AddToList(option, expression, converter, parameterIndex);
            return option;
        }

        public static void AvailableTo(this ComplexOption option, string[] contexts, OptionRequirement? requirement = null, int minOccurences = 0,
            int maxOccurences = 1, int? expectedOccurences = null, int minParameters = 0, int maxParameters = 0,
            int? expectedParameters = null)
        {
            //TODO:
        }
        #endregion
    }
}
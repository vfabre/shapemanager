#region --- License & Copyright Notice ---
/*
ConsoleFx CommandLine Processing Library

Copyright (c) 2006-2012 Jeevan James
All rights reserved.

The contents of this file are made available under the terms of the
Eclipse Public License v1.0 (the "License") which accompanies this
distribution, and is available at the following URL:
http://opensource.org/licenses/eclipse-1.0.txt

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either expressed or implied. See the License for
the specific language governing rights and limitations under the License.

By using this software in any fashion, you are agreeing to be bound by the
terms of the License.
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq.Expressions;

using ConsoleFx.Parsers;
using ConsoleFx.Resources;
using ConsoleFx.Validators;

namespace ConsoleFx.Programs.Simple
{
    public static class SimpleFluentExtensions
    {
        #region Argument extensions
        public static SimpleArgument ValidateWith(this SimpleArgument argument, params BaseValidator[] validators)
        {
            FluentArgumentHelpers.ValidateWith(argument, validators);
            return argument;
        }

        public static void HandledBy(this SimpleArgument argument, ArgumentHandler handler)
        {
            FluentArgumentHelpers.HandledBy(argument, handler);
        }

        public static void AssignTo<T>(this SimpleArgument argument, Expression<Func<T>> expression, Converter<string, T> converter = null)
        {
            FluentArgumentHelpers.AssignTo(argument, expression, converter);
        }
        #endregion

        #region Option extensions
        public static SimpleOption ValidateWith(this SimpleOption option, params BaseValidator[] validators)
        {
            if (option.NormalUsage.MaxParameters == 0)
            {
                throw new ConsoleProgramException(ConsoleProgramException.Codes.CannotAddValidatorNoParametersDefined,
                    ConsoleProgramMessages.CannotAddValidatorNoParametersDefined, option.Name);
            }
            FluentOptionHelpers.ValidateWith(option, validators);
            return option;
        }

        public static SimpleOption ValidateWith(this SimpleOption option, int parameterIndex, params BaseValidator[] validators)
        {
            if (parameterIndex >= option.NormalUsage.MaxParameters)
            {
                throw new ConsoleProgramException(ConsoleProgramException.Codes.CannotAddValidatorInvalidParameterIndex,
                    ConsoleProgramMessages.CannotAddValidatorInvalidParameterIndex, parameterIndex, option.Name);
            }
            FluentOptionHelpers.ValidateWith(option, parameterIndex, validators);
            return option;
        }

        public static void HandledBy(this SimpleOption option, OptionHandler handler)
        {
            FluentOptionHelpers.HandledBy(option, handler);
        }

        public static void Flag(this SimpleOption option, Expression<Func<bool>> expression)
        {
            FluentOptionHelpers.Flag(option, expression);
        }

        public static void AssignTo<T>(this SimpleOption option, Expression<Func<T>> expression, Converter<string, T> converter = null,
            int parameterIndex = ParameterIndex.First)
        {
            FluentOptionHelpers.AssignTo(option, expression, converter, parameterIndex);
        }

        public static void AddToList<T>(this SimpleOption option, Expression<Func<IList<T>>> expression,
            Converter<string, T> converter = null, int parameterIndex = ParameterIndex.All)
        {
            FluentOptionHelpers.AddToList(option, expression, converter, parameterIndex);
        }
        #endregion
    }
}
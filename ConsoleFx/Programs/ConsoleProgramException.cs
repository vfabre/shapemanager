#region --- License & Copyright Notice ---
/*
ConsoleFx CommandLine Processing Library

Copyright (c) 2006-2012 Jeevan James
All rights reserved.

The contents of this file are made available under the terms of the
Eclipse Public License v1.0 (the "License") which accompanies this
distribution, and is available at the following URL:
http://opensource.org/licenses/eclipse-1.0.txt

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either expressed or implied. See the License for
the specific language governing rights and limitations under the License.

By using this software in any fashion, you are agreeing to be bound by the
terms of the License.
*/
#endregion

using System;
using System.Runtime.Serialization;

using ConsoleFx.Parsers;

namespace ConsoleFx.Programs
{
    [Serializable]
    public class ConsoleProgramException : ParserException
    {
        public ConsoleProgramException(int errorCode)
            : base(errorCode)
        {
        }

        public ConsoleProgramException(int errorCode, string message, params object[] args)
            : base(errorCode, message, args)
        {
        }

        public ConsoleProgramException(int errorCode, Exception innerException, string message, params object[] args)
            : base(errorCode, innerException, message, args)
        {
        }

        internal ConsoleProgramException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        #region Codes constants inner class
        public new static class Codes
        {
            public const int CannotAddValidatorNoParametersDefined = PublicErrorCodeBase + 1;
            public const int CannotAddValidatorInvalidParameterIndex = PublicErrorCodeBase + 2;
            private const int PublicErrorCodeBase = 100;

            public const int ConverterNotFound = InternalErrorCodeBase - 1;
            public const int TypeMemberNotData = InternalErrorCodeBase - 2;
            public const int ContextNotAvailable = InternalErrorCodeBase - 3;
            private const int InternalErrorCodeBase = -100;
        }
        #endregion
    }
}
#region --- License & Copyright Notice ---
/*
ConsoleFx CommandLine Processing Library

Copyright (c) 2006-2012 Jeevan James
All rights reserved.

The contents of this file are made available under the terms of the
Eclipse Public License v1.0 (the "License") which accompanies this
distribution, and is available at the following URL:
http://opensource.org/licenses/eclipse-1.0.txt

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either expressed or implied. See the License for
the specific language governing rights and limitations under the License.

By using this software in any fashion, you are agreeing to be bound by the
terms of the License.
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;

using ConsoleFx.Resources;

namespace ConsoleFx.Validators
{
    public sealed class LookupValidator : SingleMessageValidator
    {
        private readonly List<string> _items;

        public LookupValidator(IEnumerable<string> items)
            : base(ValidationMessages.Lookup)
        {
            _items = items != null ? new List<string>(items) : new List<string>();
        }

        public void Add(string item)
        {
            _items.Add(item);
        }

        public void AddRange(IEnumerable<string> items)
        {
            _items.AddRange(items);
        }

        public override void Validate(string parameterValue)
        {
            StringComparison comparison = CaseSensitive ? StringComparison.Ordinal : StringComparison.OrdinalIgnoreCase;
            if (!_items.Any(item => parameterValue.Equals(item, comparison)))
                ValidationFailed(parameterValue);
        }

        public bool CaseSensitive { get; set; }
    }
}
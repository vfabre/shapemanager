#region --- License & Copyright Notice ---
/*
ConsoleFx CommandLine Processing Library

Copyright (c) 2006-2012 Jeevan James
All rights reserved.

The contents of this file are made available under the terms of the
Eclipse Public License v1.0 (the "License") which accompanies this
distribution, and is available at the following URL:
http://opensource.org/licenses/eclipse-1.0.txt

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either expressed or implied. See the License for
the specific language governing rights and limitations under the License.

By using this software in any fashion, you are agreeing to be bound by the
terms of the License.
*/
#endregion

using System.Text.RegularExpressions;

using ConsoleFx.Resources;

namespace ConsoleFx.Validators
{
    public sealed class RegexValidator : SingleMessageValidator
    {
        private readonly Regex _regex;

        public RegexValidator(Regex regex)
            : base(ValidationMessages.Regex)
        {
            _regex = regex;
        }

        public RegexValidator(string pattern)
            : base(ValidationMessages.Regex)
        {
            _regex = new Regex(pattern);
        }

        public override void Validate(string parameterValue)
        {
            if (!_regex.IsMatch(parameterValue))
                ValidationFailed(parameterValue);
        }

        public Regex Regex
        {
            get { return _regex; }
        }
    }

    public static class RegexPattern
    {
        public const string Email = @"^[^_][a-zA-Z0-9_]+[^_]@{1}[a-z]+[.]{1}(([a-z]{2,3})|([a-z]{2,3}[.]{1}[a-z]{2,3}))$";
        public const string FileMask = @"^[\w\.\*\?][\w\s\.\*\?]*$";
        public const string Url = @"";

        private static string _path;

        public static string Path
        {
            get { return _path ?? (_path = BuildPathRegex()); }
        }

        private static string BuildPathRegex()
        {
            //TODO:
            return string.Empty;
        }
    }
}

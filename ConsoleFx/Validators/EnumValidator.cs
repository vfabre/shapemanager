#region --- License & Copyright Notice ---
/*
ConsoleFx CommandLine Processing Library

Copyright (c) 2006-2012 Jeevan James
All rights reserved.

The contents of this file are made available under the terms of the
Eclipse Public License v1.0 (the "License") which accompanies this
distribution, and is available at the following URL:
http://opensource.org/licenses/eclipse-1.0.txt

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either expressed or implied. See the License for
the specific language governing rights and limitations under the License.

By using this software in any fashion, you are agreeing to be bound by the
terms of the License.
*/
#endregion

using System;
using System.Linq;

using ConsoleFx.Resources;

namespace ConsoleFx.Validators
{
    public class EnumValidator : SingleMessageValidator
    {
        private readonly Type _enumType;
        private readonly bool _ignoreCase;

        public EnumValidator(Type enumType, bool ignoreCase = true)
            : base(ValidationMessages.Enum)
        {
            if (enumType == null)
                throw new ArgumentNullException("enumType");
            if (!enumType.IsEnum)
                throw new ArgumentException(MiscMessages.InvalidEnumType, "enumType");
            _enumType = enumType;
            _ignoreCase = ignoreCase;
        }

        public override void Validate(string parameterValue)
        {
            string[] enumNames = Enum.GetNames(_enumType);
            StringComparison comparison = _ignoreCase ? StringComparison.OrdinalIgnoreCase : StringComparison.Ordinal;
            if (!enumNames.Any(enumName => parameterValue.Equals(enumName, comparison)))
                ValidationFailed(parameterValue);
        }

        public Type EnumType
        {
            get { return _enumType; }
        }

        public bool IgnoreCase
        {
            get { return _ignoreCase; }
        }
    }

    public sealed class EnumValidator<TEnum> : EnumValidator
        where TEnum : struct
    {
        public EnumValidator(bool ignoreCase = true)
            : base(typeof(TEnum), ignoreCase)
        {
        }
    }
}
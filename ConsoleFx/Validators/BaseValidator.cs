#region --- License & Copyright Notice ---
/*
ConsoleFx CommandLine Processing Library

Copyright (c) 2006-2012 Jeevan James
All rights reserved.

The contents of this file are made available under the terms of the
Eclipse Public License v1.0 (the "License") which accompanies this
distribution, and is available at the following URL:
http://opensource.org/licenses/eclipse-1.0.txt

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either expressed or implied. See the License for
the specific language governing rights and limitations under the License.

By using this software in any fashion, you are agreeing to be bound by the
terms of the License.
*/
#endregion

using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ConsoleFx.Validators
{
    public abstract class BaseValidator
    {
        public abstract void Validate(string parameterValue);
    }

    /// <summary>
    /// Collection of validator classes
    /// </summary>
    public sealed class ValidatorCollection : Collection<BaseValidator>
    {
        public void AddRange(IEnumerable<BaseValidator> validators)
        {
            foreach (BaseValidator validator in validators)
                Add(validator);
        }

        public void AddRange(params BaseValidator[] validators)
        {
            foreach (BaseValidator validator in validators)
                Add(validator);
        }
    }

    /// <summary>
    /// Few constants representing the index of a parameter for an option.
    /// </summary>
    /// <remarks>
    /// The parameter index needs to be specified when assigning a validator to an option. It indicates
    /// the index of the parameter whose value it must check using the validator.
    /// A special value of All (-1) represents all parameters.
    /// Unfortunately, at this time, there is no way to specify a range of parameter indices, although
    /// this is a very rare requirement.
    /// </remarks>
    public static class ParameterIndex
    {
        public const int All = -1;
        public const int First = 0;
        public const int Second = 1;
        public const int Third = 2;
        public const int Fourth = 3;
    }
}
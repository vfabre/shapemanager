#region --- License & Copyright Notice ---
/*
ConsoleFx CommandLine Processing Library

Copyright (c) 2006-2012 Jeevan James
All rights reserved.

The contents of this file are made available under the terms of the
Eclipse Public License v1.0 (the "License") which accompanies this
distribution, and is available at the following URL:
http://opensource.org/licenses/eclipse-1.0.txt

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either expressed or implied. See the License for
the specific language governing rights and limitations under the License.

By using this software in any fashion, you are agreeing to be bound by the
terms of the License.
*/
#endregion

using ConsoleFx.Resources;

namespace ConsoleFx.Validators
{
    public sealed class IntegerValidator : MultipleMessageValidator
    {
        private readonly long _minimumValue;
        private readonly long _maximumValue;

        public IntegerValidator(long minimumValue = long.MinValue, long maximumValue = long.MaxValue)
        {
            _minimumValue = minimumValue;
            _maximumValue = maximumValue;

            NotAnIntegerMessage = ValidationMessages.Integer_NotAnInteger;
            OutOfRangeMessage = ValidationMessages.Integer_OutOfRange;
        }

        public string NotAnIntegerMessage { get; set; }
        public string OutOfRangeMessage { get; set; }

        public override void Validate(string parameterValue)
        {
            long value;
            if (!long.TryParse(parameterValue, out value))
                ValidationFailed(NotAnIntegerMessage, parameterValue);
            if (value < _minimumValue || value > _maximumValue)
                ValidationFailed(OutOfRangeMessage, parameterValue);
        }
    }
}
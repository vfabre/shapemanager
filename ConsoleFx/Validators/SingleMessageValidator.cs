﻿#region --- License & Copyright Notice ---
/*
ConsoleFx CommandLine Processing Library

Copyright (c) 2006-2012 Jeevan James
All rights reserved.

The contents of this file are made available under the terms of the
Eclipse Public License v1.0 (the "License") which accompanies this
distribution, and is available at the following URL:
http://opensource.org/licenses/eclipse-1.0.txt

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either expressed or implied. See the License for
the specific language governing rights and limitations under the License.

By using this software in any fashion, you are agreeing to be bound by the
terms of the License.
*/
#endregion

using System.Globalization;

using ConsoleFx.Parsers;

namespace ConsoleFx.Validators
{
    /// <summary>
    /// Base class for validators that only do a single check and hence can only produce a single error
    /// message.
    /// For such validators, you can set the ErrorMessage property to customize the exception message
    /// in the thrown validation exception.
    /// </summary>
    public abstract class SingleMessageValidator : BaseValidator
    {
        //Derived concrete classes will pass in a default error message from the ValidationMessages.resx
        //resource file. However, these are very generic messages, and in almost cases, it is better
        //to specify a custom message using the ErrorMessage property.
        protected SingleMessageValidator(string errorMessage)
        {
            ErrorMessage = errorMessage;
        }

        /// <summary>
        /// Custom error message that is assigned to the validation failure exception
        /// </summary>
        /// <remarks>
        /// The error message can have one placeholder parameter representing the parameter value.
        /// </remarks>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Shortcut method for throwing a failed validation exception. Use this from derived classes,
        /// instead of throwing the exception directly
        /// </summary>
        /// <param name="parameterValue">The parameter value that caused the validation to fail</param>
        protected void ValidationFailed(string parameterValue)
        {
            throw new ParserException(ParserException.Codes.ValidationFailed,
                string.Format(CultureInfo.CurrentCulture, ErrorMessage, parameterValue));
        }
    }
}
#region --- License & Copyright Notice ---
/*
ConsoleFx CommandLine Processing Library

Copyright (c) 2006-2012 Jeevan James
All rights reserved.

The contents of this file are made available under the terms of the
Eclipse Public License v1.0 (the "License") which accompanies this
distribution, and is available at the following URL:
http://opensource.org/licenses/eclipse-1.0.txt

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either expressed or implied. See the License for
the specific language governing rights and limitations under the License.

By using this software in any fashion, you are agreeing to be bound by the
terms of the License.
*/
#endregion

using System;
using System.Runtime.Serialization;

namespace ConsoleFx.Parsers
{
    [Serializable]
    public class ParserException : ConsoleFxException
    {
        protected ParserException(int errorCode)
            : base(errorCode)
        {
        }

        public ParserException(int errorCode, string message, params object[] args)
            : base(errorCode, message, args)
        {
        }

        protected ParserException(int errorCode, Exception innerException, string message, params object[] args)
            : base(errorCode, innerException, message, args)
        {
        }

        internal ParserException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        #region Codes constants inner class
        public static class Codes
        {
            public const int ValidationFailed = PublicErrorCodeBase + 1;
            public const int InvalidOptionSpecified = PublicErrorCodeBase + 2;
            public const int InvalidOptionParametersSpecified = PublicErrorCodeBase + 3;
            public const int RequiredOptionAbsent = PublicErrorCodeBase + 4;
            public const int OptionsBeforeParameters = PublicErrorCodeBase + 5;
            public const int OptionsAfterParameters = PublicErrorCodeBase + 6;
            public const int TooFewOptions = PublicErrorCodeBase + 7;
            public const int TooManyOptions = PublicErrorCodeBase + 8;
            public const int InvalidOptionParameterSpecifier = PublicErrorCodeBase + 9;
            public const int InvalidNumberOfArguments = PublicErrorCodeBase + 10;
            public const int RequiredParametersAbsent = PublicErrorCodeBase + 11;
            public const int InvalidParametersSpecified = PublicErrorCodeBase + 12;

            public const int RequiredArgumentsDefinedAfterOptional = InternalErrorCodeBase - 1;

            private const int PublicErrorCodeBase = 0;
            private const int InternalErrorCodeBase = 0;
        }
        #endregion
    }
}
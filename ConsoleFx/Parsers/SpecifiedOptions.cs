#region --- License & Copyright Notice ---
/*
ConsoleFx CommandLine Processing Library

Copyright (c) 2006-2012 Jeevan James
All rights reserved.

The contents of this file are made available under the terms of the
Eclipse Public License v1.0 (the "License") which accompanies this
distribution, and is available at the following URL:
http://opensource.org/licenses/eclipse-1.0.txt

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either expressed or implied. See the License for
the specific language governing rights and limitations under the License.

By using this software in any fashion, you are agreeing to be bound by the
terms of the License.
*/
#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace ConsoleFx.Parsers
{
    //Contains the list of options actually specified on the command line.
    //Option name -> SpecifiedOptionParametersCollection
    [Serializable]
    public sealed class SpecifiedOptions : Dictionary<string, SpecifiedOptionParametersCollection>
    {
        public SpecifiedOptions()
        {
        }

        private SpecifiedOptions(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public SpecifiedOptionParametersCollection this[Option option]
        {
            get
            {
                StringComparison comparison = option.CaseSensitive ? StringComparison.Ordinal : StringComparison.OrdinalIgnoreCase;
                foreach (KeyValuePair<string, SpecifiedOptionParametersCollection> kvp in this)
                {
                    if (option.Name.Equals(kvp.Key, comparison) ||
                        (!string.IsNullOrEmpty(option.ShortName) && option.ShortName.Equals(kvp.Key, comparison)))
                        return kvp.Value;
                }
                return null;
            }
        }
    }

    //Contains the list of parameters specified for a particular switch. Since it is allowed to
    //specify the same switch multiple times on the same command line, this structure is actually a
    //collection of switch parameter collections.
    [Serializable]
    public sealed class SpecifiedOptionParametersCollection : Collection<SpecifiedOptionParameters>
    {
    }

    [Serializable]
    public sealed class SpecifiedOptionParameters : Collection<string>
    {
        private readonly string _optionName;

        public SpecifiedOptionParameters(string optionName)
        {
            _optionName = optionName;
        }

        public string OptionName
        {
            get { return _optionName; }
        }
    }
}
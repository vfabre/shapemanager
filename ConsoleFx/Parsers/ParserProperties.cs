﻿#region --- License & Copyright Notice ---
/*
ConsoleFx CommandLine Processing Library

Copyright (c) 2006-2012 Jeevan James
All rights reserved.

The contents of this file are made available under the terms of the
Eclipse Public License v1.0 (the "License") which accompanies this
distribution, and is available at the following URL:
http://opensource.org/licenses/eclipse-1.0.txt

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either expressed or implied. See the License for
the specific language governing rights and limitations under the License.

By using this software in any fashion, you are agreeing to be bound by the
terms of the License.
*/
#endregion

using System.Diagnostics;

namespace ConsoleFx.Parsers
{
    public sealed class ParserProperties
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private readonly Behaviors _behavior = new Behaviors();

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private readonly Inputs _available = new Inputs();

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private readonly Outputs _specified = new Outputs();

        public string Context { get; set; }

        public Behaviors Behavior
        {
            get { return _behavior; }
        }

        public Inputs Available
        {
            get { return _available; }
        }

        public Outputs Specified
        {
            get { return _specified; }
        }

        #region Parser behavior properties
        [DebuggerDisplay("Display usage on error: {DisplayUsageOnError}, Grouping: {Grouping}")]
        public sealed class Behaviors
        {
            public Behaviors()
            {
                DisplayUsageOnError = true;
                Grouping = CommandGrouping.DoesNotMatter;
            }

            public bool DisplayUsageOnError { get; set; }

            public CommandGrouping Grouping { get; set; }
        }
        #endregion

        #region Parser input data - arguments and options
        public sealed class Inputs
        {
            [DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private readonly Options _options = new Options();

            [DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private readonly Arguments _arguments = new Arguments();

            public Arguments Arguments
            {
                get { return _arguments; }
            }

            public Options Options
            {
                get { return _options; }
            }
        }
        #endregion

        #region Parser outputs - specified arguments and options
        [DebuggerDisplay("{Command}")]
        public sealed class Outputs
        {
            [DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private readonly SpecifiedArguments _arguments = new SpecifiedArguments();

            [DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private readonly SpecifiedOptions _options = new SpecifiedOptions();

            public string Command { get; set; }

            public SpecifiedArguments Arguments
            {
                get { return _arguments; }
            }

            public SpecifiedOptions Options
            {
                get { return _options; }
            }
        }
        #endregion
    }
}
#region --- License & Copyright Notice ---
/*
ConsoleFx CommandLine Processing Library

Copyright (c) 2006-2012 Jeevan James
All rights reserved.

The contents of this file are made available under the terms of the
Eclipse Public License v1.0 (the "License") which accompanies this
distribution, and is available at the following URL:
http://opensource.org/licenses/eclipse-1.0.txt

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either expressed or implied. See the License for
the specific language governing rights and limitations under the License.

By using this software in any fashion, you are agreeing to be bound by the
terms of the License.
*/
#endregion

using System;
using System.Runtime.Serialization;

namespace ConsoleFx.Capture
{
    [Serializable]
    public sealed class ConsoleCaptureException : ConsoleFxException
    {
        public ConsoleCaptureException(int errorCode)
            : base(errorCode)
        {
        }

        public ConsoleCaptureException(int errorCode, string message, params object[] args)
            : base(errorCode, message, args)
        {
        }

        public ConsoleCaptureException(int errorCode, Exception innerException, string message, params object[] args)
            : base(errorCode, innerException, message, args)
        {
        }

        internal ConsoleCaptureException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        #region Error codes
        public static class Codes
        {
            public const int ProcessStartFailed = ErrorCodeBase + 1;
            public const int ProcessAborted = ErrorCodeBase + 2;

            private const int ErrorCodeBase = 100;
        }
        #endregion
    }
}
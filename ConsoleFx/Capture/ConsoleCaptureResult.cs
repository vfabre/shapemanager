#region --- License & Copyright Notice ---
/*
ConsoleFx CommandLine Processing Library

Copyright (c) 2006-2012 Jeevan James
All rights reserved.

The contents of this file are made available under the terms of the
Eclipse Public License v1.0 (the "License") which accompanies this
distribution, and is available at the following URL:
http://opensource.org/licenses/eclipse-1.0.txt

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either expressed or implied. See the License for
the specific language governing rights and limitations under the License.

By using this software in any fashion, you are agreeing to be bound by the
terms of the License.
*/
#endregion

using System;
using System.Diagnostics;

namespace ConsoleFx.Capture
{
    [DebuggerDisplay("Exit code: {ExitCode}")]
    [Serializable]
    public sealed class ConsoleCaptureResult
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private readonly int _exitCode;

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private readonly string _outputMessage;

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private readonly string _errorMessage;

        //Only the ConsoleCapture class can create an instance
        internal ConsoleCaptureResult(int exitCode, string outputMessage, string errorMessage)
        {
            _exitCode = exitCode;
            _outputMessage = outputMessage;
            _errorMessage = errorMessage;
        }

        public string ErrorMessage
        {
            [DebuggerStepThrough]
            get { return _errorMessage; }
        }

        public int ExitCode
        {
            [DebuggerStepThrough]
            get { return _exitCode; }
        }

        public string OutputMessage
        {
            [DebuggerStepThrough]
            get { return _outputMessage; }
        }
    }
}
﻿#region --- License & Copyright Notice ---
/*
ConsoleFx CommandLine Processing Library

Copyright (c) 2006-2012 Jeevan James
All rights reserved.

The contents of this file are made available under the terms of the
Eclipse Public License v1.0 (the "License") which accompanies this
distribution, and is available at the following URL:
http://opensource.org/licenses/eclipse-1.0.txt

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either expressed or implied. See the License for
the specific language governing rights and limitations under the License.

By using this software in any fashion, you are agreeing to be bound by the
terms of the License.
*/
#endregion

using System.Collections.Generic;

namespace ConsoleFx.Programs.Interactive
{
    public abstract class CommandBase
    {
        public abstract void Execute();

        public virtual IEnumerable<ShellOption> GetOptions()
        {
            yield break;
        }

        public virtual IEnumerable<ShellArgument> GetArguments()
        {
            yield break;
        }

        protected ShellArgument AddArgument(bool optional = false)
        {
            var argument = new ShellArgument { IsOptional = optional };
            return argument;
        }

        protected ShellOption AddOption(string name, string shortName = null, OptionRequirement? requirement = null, int minOccurences = 0,
            int maxOccurences = 1, int? expectedOccurences = null, int minParameters = 0, int maxParameters = 0,
            int? expectedParameters = null, bool caseSensitive = false, int order = int.MaxValue)
        {
            var option = new ShellOption(name) {
                CaseSensitive = caseSensitive,
                Order = order,
            };
            if (!string.IsNullOrWhiteSpace(shortName))
                option.ShortName = shortName;

            if (requirement.HasValue)
                option.NormalUsage.Requirement = requirement.Value;
            else if (expectedOccurences.HasValue)
                option.NormalUsage.ExpectedOccurences = expectedOccurences.Value;
            else
            {
                option.NormalUsage.MinOccurences = minOccurences;
                option.NormalUsage.MaxOccurences = maxOccurences;
            }

            if (expectedParameters.HasValue)
                option.NormalUsage.ExpectedParameters = expectedParameters.Value;
            else
            {
                option.NormalUsage.MinParameters = minParameters;
                option.NormalUsage.MaxParameters = maxParameters;
            }

            option.Usages[ProgramContext.Help].Requirement = OptionRequirement.NotAllowed;

            return option;
        }
    }
}
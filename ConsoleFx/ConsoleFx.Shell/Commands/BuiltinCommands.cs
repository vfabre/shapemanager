#region --- License & Copyright Notice ---
/*
ConsoleFx CommandLine Processing Library

Copyright (c) 2006-2012 Jeevan James
All rights reserved.

The contents of this file are made available under the terms of the
Eclipse Public License v1.0 (the "License") which accompanies this
distribution, and is available at the following URL:
http://opensource.org/licenses/eclipse-1.0.txt

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either expressed or implied. See the License for
the specific language governing rights and limitations under the License.

By using this software in any fashion, you are agreeing to be bound by the
terms of the License.
*/
#endregion

using System;
using System.Collections.Generic;

namespace ConsoleFx.Programs.Interactive.Commands
{
    public enum BuiltinCommands
    {
        Cls,
        Color,
        Prompt,
    }

    internal static class BuiltinCommandMapping
    {
        internal static readonly Dictionary<BuiltinCommands, Tuple<Type, Func<InteractiveShell, CommandBase>>> Mappings = new Dictionary<BuiltinCommands, Tuple<Type, Func<InteractiveShell, CommandBase>>> {
            { BuiltinCommands.Cls, Tuple.Create(typeof(ClsCommand), (Func<InteractiveShell, CommandBase>)null) },
            { BuiltinCommands.Color, Tuple.Create(typeof(ColorCommand), (Func<InteractiveShell, CommandBase>)null) },
            { BuiltinCommands.Prompt, Tuple.Create(typeof(PromptCommand), (Func<InteractiveShell, CommandBase>)(shell => new PromptCommand(shell))) },
        };
    }
}
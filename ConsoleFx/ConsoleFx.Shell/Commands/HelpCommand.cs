﻿#region --- License & Copyright Notice ---
/*
ConsoleFx CommandLine Processing Library

Copyright (c) 2006-2012 Jeevan James
All rights reserved.

The contents of this file are made available under the terms of the
Eclipse Public License v1.0 (the "License") which accompanies this
distribution, and is available at the following URL:
http://opensource.org/licenses/eclipse-1.0.txt

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either expressed or implied. See the License for
the specific language governing rights and limitations under the License.

By using this software in any fashion, you are agreeing to be bound by the
terms of the License.
*/
#endregion

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

using ConsoleFx.Programs.Interactive.Resources;

namespace ConsoleFx.Programs.Interactive.Commands
{
    [Command(Description = "Provides help information for all available commands")]
    internal sealed class HelpCommand : CommandBase
    {
        private readonly RegisteredCommandCollection _registeredCommands;
#pragma warning disable 649
        private static string _commandName;
#pragma warning restore 649

        internal HelpCommand(RegisteredCommandCollection registeredCommands)
        {
            _registeredCommands = registeredCommands;
        }

        public override void Execute()
        {
            if (_commandName == null)
            {
                Console.WriteLine(ShellMessages.HelpLine1);
                Console.WriteLine(ShellMessages.HelpLine2);
                Console.WriteLine();

                IOrderedEnumerable<RegisteredCommand> orderedCommands = _registeredCommands.OrderBy(rc => rc.Type.Name);
                foreach (RegisteredCommand rc in orderedCommands)
                {
                    Console.Write("{0,-15}", rc.Name.ToUpper(CultureInfo.CurrentCulture));
                    if (!string.IsNullOrWhiteSpace(rc.Description))
                        ConsoleEx.WriteIndented(rc.Description, 15, true);
                    else
                        Console.WriteLine();
                }

                Console.WriteLine();
                Console.WriteLine(ShellMessages.ExitAssistance, "EXIT"); //TODO:
            }
            else
            {
                RegisteredCommand registeredCommand = _registeredCommands[_commandName];
                if (registeredCommand == null)

                    ConsoleEx.WriteLine(ConsoleColor.Red, null, ShellMessages.CommandNotRecognized, _commandName);
                else
                    Console.WriteLine(registeredCommand.HelpDescription);
            }
        }

        public override IEnumerable<ShellArgument> GetArguments()
        {
            yield return AddArgument(true)
                .AssignTo(() => _commandName);
        }
    }
}
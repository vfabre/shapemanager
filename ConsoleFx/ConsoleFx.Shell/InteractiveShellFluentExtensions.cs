﻿#region --- License & Copyright Notice ---
/*
ConsoleFx CommandLine Processing Library

Copyright (c) 2006-2012 Jeevan James
All rights reserved.

The contents of this file are made available under the terms of the
Eclipse Public License v1.0 (the "License") which accompanies this
distribution, and is available at the following URL:
http://opensource.org/licenses/eclipse-1.0.txt

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either expressed or implied. See the License for
the specific language governing rights and limitations under the License.

By using this software in any fashion, you are agreeing to be bound by the
terms of the License.
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq.Expressions;

using ConsoleFx.Parsers;
using ConsoleFx.Validators;

namespace ConsoleFx.Programs.Interactive
{
    public static class InteractiveShellFluentExtensions
    {
        #region Argument extensions
        public static ShellArgument ValidateWith(this ShellArgument argument, params BaseValidator[] validators)
        {
            FluentArgumentHelpers.ValidateWith(argument, validators);
            return argument;
        }

        public static ShellArgument HandledBy(this ShellArgument argument, ArgumentHandler handler)
        {
            FluentArgumentHelpers.HandledBy(argument, handler);
            return argument;
        }

        public static ShellArgument AssignTo<T>(this ShellArgument argument, Expression<Func<T>> expression, Converter<string, T> converter = null)
        {
            FluentArgumentHelpers.AssignTo(argument, expression, converter);
            return argument;
        }
        #endregion

        #region Option extensions
        public static ShellOption ValidateWith(this ShellOption option, params BaseValidator[] validators)
        {
            if (option.NormalUsage.MaxParameters == 0)
                throw new ConsoleProgramException(-1,
                    "Cannot add validators to the '{0}' option because it does not have any parameters defined", option.Name);
            FluentOptionHelpers.ValidateWith(option, validators);
            return option;
        }

        public static ShellOption ValidateWith(this ShellOption option, int parameterIndex, params BaseValidator[] validators)
        {
            if (parameterIndex >= option.NormalUsage.MaxParameters)
                throw new ConsoleProgramException(-1,
                    "Cannot add validators to the parameter at index {0} for the '{1}' option because that many parameters have not been defined",
                    parameterIndex, option.Name);
            FluentOptionHelpers.ValidateWith(option, parameterIndex, validators);
            return option;
        }

        public static ShellOption HandledBy(this ShellOption option, OptionHandler handler)
        {
            FluentOptionHelpers.HandledBy(option, handler);
            return option;
        }

        public static ShellOption Flag(this ShellOption option, Expression<Func<bool>> expression)
        {
            FluentOptionHelpers.Flag(option, expression);
            return option;
        }

        public static ShellOption AssignTo<T>(this ShellOption option, Expression<Func<T>> expression, Converter<string, T> converter = null,
            int parameterIndex = ParameterIndex.First)
        {
            FluentOptionHelpers.AssignTo(option, expression, converter, parameterIndex);
            return option;
        }

        public static ShellOption AddToList<T>(this ShellOption option, Expression<Func<IList<T>>> expression,
            Converter<string, T> converter = null, int parameterIndex = ParameterIndex.All)
        {
            FluentOptionHelpers.AddToList(option, expression, converter, parameterIndex);
            return option;
        }
        #endregion
    }
}
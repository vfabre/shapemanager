﻿#region --- License & Copyright Notice ---
/*
ConsoleFx CommandLine Processing Library

Copyright (c) 2006-2012 Jeevan James
All rights reserved.

The contents of this file are made available under the terms of the
Eclipse Public License v1.0 (the "License") which accompanies this
distribution, and is available at the following URL:
http://opensource.org/licenses/eclipse-1.0.txt

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either expressed or implied. See the License for
the specific language governing rights and limitations under the License.

By using this software in any fashion, you are agreeing to be bound by the
terms of the License.
*/
#endregion

using System.Text;

using ConsoleFx.Parsers;

namespace ConsoleFx.Programs.Interactive
{
    internal sealed class ShellUsageBuilder : UsageBuilder
    {
        internal ShellUsageBuilder(Parser parser)
            : base(parser)
        {
        }

        public override void GenerateOptionsUsage(StringBuilder sb)
        {
        }

        protected override void GenerateHeaderSection(StringBuilder sb)
        {
            throw new System.NotImplementedException();
        }

        protected override void GenerateFooterSection(StringBuilder sb)
        {
            throw new System.NotImplementedException();
        }
    }
}
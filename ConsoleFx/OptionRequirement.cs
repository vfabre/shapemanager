#region --- License & Copyright Notice ---
/*
ConsoleFx CommandLine Processing Library

Copyright (c) 2006-2012 Jeevan James
All rights reserved.

The contents of this file are made available under the terms of the
Eclipse Public License v1.0 (the "License") which accompanies this
distribution, and is available at the following URL:
http://opensource.org/licenses/eclipse-1.0.txt

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either expressed or implied. See the License for
the specific language governing rights and limitations under the License.

By using this software in any fashion, you are agreeing to be bound by the
terms of the License.
*/
#endregion

namespace ConsoleFx
{
    /// <summary>
    /// Specifies whether an option is required, optional or not allowed for a program context.
    /// </summary>
    public enum OptionRequirement
    {
        /// <summary>
        /// The option is optional (this is the default). Sets the option's MinOccurences property to 0 (zero) and MaxOccurences
        /// property to 1 (one). However, the MaxOccurence value can be increased, and as long as the MinOccurence
        /// value is zero, it will be considered optional.
        /// </summary>
        Optional,

        /// <summary>
        /// The option is required. Sets the option's MinOccurences and MaxOccurences properties to 1 (one).
        /// </summary>
        Required,

        /// <summary>
        /// The option is not allowed. Sets the option's MinOccurences and MaxOccurences properties to 0 (zero).
        /// </summary>
        NotAllowed,
    }
}
#region --- License & Copyright Notice ---
/*
ConsoleFx CommandLine Processing Library

Copyright (c) 2006-2012 Jeevan James
All rights reserved.

The contents of this file are made available under the terms of the
Eclipse Public License v1.0 (the "License") which accompanies this
distribution, and is available at the following URL:
http://opensource.org/licenses/eclipse-1.0.txt

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either expressed or implied. See the License for
the specific language governing rights and limitations under the License.

By using this software in any fashion, you are agreeing to be bound by the
terms of the License.
*/
#endregion

using System;
using System.Globalization;
using System.Runtime.Serialization;

namespace ConsoleFx
{
    [Serializable]
    public abstract class ConsoleFxException : Exception
    {
        private readonly int _errorCode;

        protected ConsoleFxException(int errorCode)
        {
            _errorCode = errorCode;
        }

        protected ConsoleFxException(int errorCode, string message, params object[] args)
            : base(string.Format(CultureInfo.CurrentCulture, message, args))
        {
            _errorCode = errorCode;
        }

        protected ConsoleFxException(int errorCode, Exception innerException, string message, params object[] args)
            : base(string.Format(CultureInfo.CurrentCulture, message, args), innerException)
        {
            _errorCode = errorCode;
        }

        public int ErrorCode
        {
            get { return _errorCode; }
        }

        protected ConsoleFxException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}